'use strict';
var gulp = require('gulp');
var requireDir = require('require-dir');
//var clean = require('gulp-clean');

// Tasks directory
var tasks = requireDir('./gulp');

// Development tasks
gulp.task('serve', tasks.browserSync); //BrowserSync
gulp.task('sass', tasks.sass); // SASS files, PostCSS, autoprefixer
gulp.task('postCss', tasks.postCss); // PostCSS, autoprefixer
gulp.task('svgImages', tasks.svgImages); // Svg images
gulp.task('svgIcons', tasks.svgIcons); // Svg images
gulp.task('images', tasks.images); // Images
gulp.task('javascript', tasks.javascript); // Images
gulp.task('copy', tasks.copy); // Copy - flexboxgrid, sanitize
gulp.task('templates', tasks.templates); // twig > html
gulp.task('fonts', tasks.fonts); // twig > html


gulp.task('default', ['fonts','copy','sass','svgImages','images','javascript','templates','serve']);

// Build tasks


gulp.task('build', ['copy','sass','postCss','svgImages','images','copy','javascript']);


gulp.task('watch', function() {
  gulp.watch('./assets/sass/**/*.scss', ['sass'])
});
