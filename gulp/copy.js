var gulp = require('gulp');
var gulpCopy = require('gulp-copy');
var rename = require("gulp-rename");

module.exports = function () {
    gulp.src('./node_modules/bootstrap/scss/**/*.scss')
    .pipe(gulp.dest('assets/sass/vendor/bootstrap'));
};
