var gulp = require('gulp');
var twig = require('gulp-twig');
var fs = require('fs-extra');
var faker = require('gulp-faker');
var json = JSON.parse(fs.readFileSync('./assets/data/data.json'));

// Compile Twig templates to HTML
module.exports = function () {
    return gulp.src('assets/templates/*.twig') // run the Twig template parser on all .twig files in the "src" directory
        .pipe(twig({data: json}))
        .pipe(faker())
        .pipe(gulp.dest('./')); // output the rendered HTML files to the root directory
};
