var gulp = require('gulp');

// Copy font files
module.exports = function () {
        gulp.src('assets/fonts/**/*') 
        .pipe(gulp.dest('./dist/fonts'));
};
