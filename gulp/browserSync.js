var gulp = require('gulp');
var browserSync  = require("browser-sync").create();
var htmlInjector = require("bs-html-injector");
/**
 * Start Browsersync
 */
module.exports = function () {

    browserSync.init({
        server: './',
        browser: ['google chrome', 'firefox']
    });

    gulp.watch("assets/sass/**/*.scss", ['sass']);
    gulp.watch("dist/css/*.css").on('change', browserSync.reload);
    gulp.watch("assets/js/*.js", ['javascript']);
    gulp.watch("assets/templates/**/*.twig", ['templates']);
    gulp.watch("*.html").on('change', browserSync.reload);
    gulp.watch("assets/images/**/*.*", ['images']);
    gulp.watch("assets/svg/**/*.*", ['svgImages']);
};
