var gulp = require('gulp');
var svgmin = require('gulp-svgmin');
var raster = require('gulp-raster');
var rename = require('gulp-rename');

module.exports = function () {
  gulp.src('./assets/svg/*.svg')
  .pipe(svgmin({
    plugins: [{
        removeDoctype: true
    }, {
        removeViewBox: true
    }]
  }))
  .pipe(gulp.dest('./dist/svg/')); // Filter out everything except the SVG file

};
